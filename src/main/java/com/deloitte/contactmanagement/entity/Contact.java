package com.deloitte.contactmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
@SQLDelete(sql = "UPDATE contact SET deleted = 1 WHERE contact_id=?")
@Where(clause = "deleted=0")
public class Contact {
    @Id
    @GeneratedValue
    private Long contactId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String addressLine;
    private String cityName;
    private LocalDate birthDate;
    private String email;

    @JsonIgnore
    private boolean deleted = Boolean.FALSE;

    private Long userId;


    public Contact(String firstName,String lastName, String phoneNumber, String addressLine,String cityName, LocalDate birthDate, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.addressLine = addressLine;
        this.cityName = cityName;
        this.birthDate = birthDate;
        this.email = email;
    }
}
