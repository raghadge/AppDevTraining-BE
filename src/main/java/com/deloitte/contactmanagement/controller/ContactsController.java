package com.deloitte.contactmanagement.controller;


import com.deloitte.contactmanagement.entity.Contact;
import com.deloitte.contactmanagement.entity.User;
import com.deloitte.contactmanagement.exception.ContactNotFoundException;
import com.deloitte.contactmanagement.service.ContactService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ContactsController {

    private ContactService contactService;

    public ContactsController(ContactService contactService){
        this.contactService = contactService;
    }


    //long userid = (User)authentication.getPrincipal()).getId();
    //@CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/api/contacts")
    public List<Contact> getAllContacts(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return contactService.getContactsByUserId(((User)authentication.getPrincipal()).getId());

        //return contactService.getAllContacts();
    }

    @GetMapping(value = "/api/contacts/id/{id}")
    public Contact getContact(@PathVariable Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return contactService.getContactById(((User)authentication.getPrincipal()).getId(),id).orElseThrow(() -> new ContactNotFoundException(id));
    }//sel * wher first=Bond

    @GetMapping(value = "/api/contacts/{firstName}")
    public List<Contact> getContactByName(@PathVariable String firstName){ // @pathPa sting
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return contactService.getContactByName(((User)authentication.getPrincipal()).getId(),firstName) ;//.orElseThrow(() -> new ContactNotFoundException(firstName));
    }

    @PostMapping(value = "/api/contacts")
    public ResponseEntity<?> addContact(@RequestBody Contact newContact) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        newContact.setUserId(((User)authentication.getPrincipal()).getId());
        Contact addedContact = contactService.addContact(newContact);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(addedContact.getContactId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/api/contacts/{id}")
    public Contact updateContact(@RequestBody Contact newContact, @PathVariable Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        newContact.setUserId(((User)authentication.getPrincipal()).getId());
        return contactService.updateContact(((User)authentication.getPrincipal()).getId(),newContact, id);
    }

    @DeleteMapping(value = "/api/contacts/{id}")
    public ResponseEntity<?> deleteContact(@PathVariable Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return contactService.deleteContact(((User)authentication.getPrincipal()).getId(),id) ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}

