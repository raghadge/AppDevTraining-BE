package com.deloitte.contactmanagement.repository;

import com.deloitte.contactmanagement.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author raghadge
 */
public interface ContactRepository extends JpaRepository<Contact,Long> {
    List<Contact> findAll();
    List<Contact> findByUserId(Long userid);

    List<Contact> findByUserIdAndFirstName(Long userId, String firstName);
    Optional<Contact>findByUserIdAndContactId(Long userId,Long id);

    //void deleteByUserIdAndId(Long userId, Long id);

    Optional<Contact> findByContactId(Long id);

//    void deleteByContactId(Long id);

//d     void deleteByUserIdAndContactId(Long userId, Long contactId);

    //void deleteUserIdAndContactId(Long userId, Long contactId);
}
