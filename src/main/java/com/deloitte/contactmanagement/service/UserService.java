package com.deloitte.contactmanagement.service;


import com.deloitte.contactmanagement.entity.Role;
import com.deloitte.contactmanagement.entity.User;
import com.deloitte.contactmanagement.entity.UserDataDTO;
import com.deloitte.contactmanagement.repository.UserRepository;
import com.deloitte.contactmanagement.security.JWTAuthenticationFilter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private RoleService roleService;
//    @Autowired
//    private ModelMapper modelMapper;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    public String signUp(UserDataDTO userDto) {
        User user = modelMapper().map(userDto, User.class);
        user.setPassword(encoder.encode(userDto.getPassword()));

        Role role = roleService.findByName("USER");
        List<Role> roleSet = new ArrayList<>();
        roleSet.add(role);

        if(user.getEmail().split("@")[1].equals("admin.com")){
            role = roleService.findByName("ADMIN");
            roleSet.add(role);
        }

        user.setRoles(roleSet);
        // After sing up save user detail in users db
        user = userRepository.save(user);
        return JWTAuthenticationFilter.generateToken(user.getEmail());
    }

//    public String login(UserDataDTO user) {
//
//        final Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        user.getEmail(),
//                        user.getPassword()
//                )
//        );
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        final String token = JWTAuthenticationFilter.generateToken(user.getEmail());
//                //jwtTokenUtil.generateToken(authentication);
//        return token;//ResponseEntity.ok(new AuthToken(token));
//    }
}
