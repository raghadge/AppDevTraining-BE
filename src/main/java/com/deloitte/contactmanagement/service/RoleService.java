package com.deloitte.contactmanagement.service;

import com.deloitte.contactmanagement.entity.Role;
import com.deloitte.contactmanagement.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author raghadge
 */
@Service
public class RoleService {
    @Autowired
    private RoleRepository roleDao;

    public Role findByName(String name) {
        Role role = roleDao.findRoleByName(name);
        return role;
    }
}
