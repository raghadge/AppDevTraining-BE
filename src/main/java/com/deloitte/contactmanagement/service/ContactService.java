package com.deloitte.contactmanagement.service;


import com.deloitte.contactmanagement.repository.ContactRepository;
import com.deloitte.contactmanagement.entity.Contact;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactService {
    private ContactRepository contactsRepository;

    public ContactService(ContactRepository contactsRepository) {
        this.contactsRepository = contactsRepository;
    }

    public List<Contact> getContactsByUserId(Long userId){
        //return contactsRepository.findByUserid(userId);
        return contactsRepository.findByUserId(userId);
//        return contactsRepository.findAll();
    }

    public List<Contact> getAllContacts(){
        return contactsRepository.findAll();
    }

    public List<Contact> getContactByName(Long userId,String firstName){
        return contactsRepository.findByUserIdAndFirstName(userId,firstName);
    }

    public Contact addContact(Contact contact) {
        return contactsRepository.save(contact);
    }

    public Contact updateContact(Long userId,Contact newContact, Long id) {
        return contactsRepository.findByUserIdAndContactId(userId,id).map(contact -> {
            contact.setFirstName(newContact.getFirstName());
            contact.setLastName(newContact.getLastName());
            contact.setPhoneNumber(newContact.getPhoneNumber());
            contact.setAddressLine(newContact.getAddressLine());
            contact.setBirthDate(newContact.getBirthDate());
            contact.setCityName(newContact.getCityName());
            contact.setEmail(newContact.getEmail());
            return contactsRepository.save(contact);
        }).orElseGet(() -> {
            newContact.setContactId(id);
            return contactsRepository.save(newContact);
        });
    }

    public boolean deleteContact(Long userId, Long contactId) {
        try {
            //contactsRepository.deleteByUserIdAndContactId(userId,contactId);
            contactsRepository.deleteById(contactId);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public Optional<Contact> getContactById(Long userId, Long contactId) {
        return contactsRepository.findByUserIdAndContactId(userId,contactId);
    }
}
